
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
		'id'=>'comment-form',
		//'type' => 'horizontal',
		'enableAjaxValidation'=>true,
)); ?>
<fieldset>
	<p class="note">
		Fields with <span class="required">*</span> are required.
	</p>

	<?php echo $form->textFieldGroup($model,'author',array('size'=>60,'maxlength'=>128)); ?>

	<?php echo $form->textFieldGroup($model,'email'); ?>

	<?php echo $form->textAreaGroup($model,'content'); ?>


</fieldset>
<?php $this->widget(
		'booster.widgets.TbButton',
		array('buttonType' => 'reset', 'label' => 'Reset')
		); ?>
<?php $this->widget( 'booster.widgets.TbButton',array(
		'buttonType' =>	'submit',
		'context' => 'primary',
		'label' => $model->isNewRecord ? 'Create' : 'Save')
			);?>
<?php $this->endWidget(); ?>

