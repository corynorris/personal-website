<?php
$this->pageTitle= Yii::app()->name . ' - ' . Yii::t('app',$model->title);
?>

<h1>Update Comment #<?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>