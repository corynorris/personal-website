<?php 
$this->pageTitle=Yii::app()->name . ' - ' . Yii::t('app','Comments');
?>

<h1>Comments</h1>

<?php $this->widget('booster.widgets.TbListView',array(
		'dataProvider'=>$dataProvider,
		'itemView'=>'_view',
		'template'=>"{items}\n{pager}",
)); ?>