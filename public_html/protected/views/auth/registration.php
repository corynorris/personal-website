<?php
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('app', "Registration");
?>
<div class="jumbotron masthead">
    <div class="container">
        <h1><?php echo Yii::t('app', "Registration"); ?></h1>     
    </div>
</div>
<div class="container">
    <?php
    $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'id' => 'registration-form',
        'htmlOptions' => array('class' => 'well'),
    ));
    ?>  
    <p><?php echo Helper::t('Fields with <span class="required">*</span> are required.'); ?></p>
    <?php echo $form->errorSummary($model); ?>
    <?php echo $form->textFieldGroup($model, 'username', array('autocomplete' => 'off')); ?>
    <?php echo $form->passwordFieldGroup($model, 'password', array('class' => 'form-control')); ?>
    <?php echo $form->passwordFieldGroup($model, 'verifyPassword', array('class' => 'form-control')); ?>
    <?php echo $form->emailFieldGroup($model, 'email', array('autocomplete' => 'off')); ?>
    <?php
    echo $form->textFieldGroup($model, 'verifyCode', array(
        'help' => 'Please enter the letters as they are shown in the image above.',
        'controlOptions' => array('before' => $this->widget('system.web.widgets.captcha.CCaptcha', array(), true) . '<br/>'),
    ));
    ?>

	<?php $this->widget( 'booster.widgets.TbButton',array(
			'buttonType' =>	'submit',
			'context' => 'primary',
			'label' => 'Register')
			);?>

	<?php $this->endWidget(); ?>
</div>



