<?php $this->pageTitle = Yii::app()->name . ' - ' . Yii::t('app', 'Login'); ?>
<div class="jumbotron masthead">
	<div class="container">
		<h1>
			<?php echo Yii::t('app', 'Login'); ?>
		</h1>
	</div>
</div>
<div class="container">

	<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
			'id'=>'login-form',
			'htmlOptions' => array('class' => 'well'),
)); ?>
	<p>
		<?php echo Helper::t('Fields with <span class="required">*</span> are required.'); ?>
	</p>
	<?php echo $form->errorSummary($model); ?>
	<?php echo $form->textFieldGroup($model, 'username', array('autocomplete' => 'off')); ?>
	<?php echo $form->passwordFieldGroup($model, 'password', array('autocomplete' => 'off')); ?>
	<?php echo $form->checkboxGroup($model, 'rememberMe'); ?>

	<?php $this->widget( 'booster.widgets.TbButton',array(
			'buttonType' =>	'submit',
			'context' => 'primary',
			'label' => 'Login')
			);?>

	<?php $this->endWidget(); ?>

</div>

