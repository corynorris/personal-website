<?php $this->pageTitle = Yii::app()->name . ' - ' . Yii::t('app', 'Restore'); ?>
<div class="jumbotron masthead">
    <div class="container">
        <h1><?php echo Yii::t('app', 'Restore'); ?></h1>
    </div>
</div>
<div class="container"> 
    <?php
    $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'id' => 'recovery-form',
        'htmlOptions' => array('class' => 'well'),
    ));
    ?>  
    <p><?php echo Helper::t('Fields with <span class="required">*</span> are required.'); ?></p>
    <?php echo $form->errorSummary($model); ?>
    <?php echo $form->textFieldGroup($model, 'login_or_email', array('autocomplete' => 'off')); ?>



	<?php $this->widget( 'booster.widgets.TbButton',array(
			'buttonType' =>	'submit',
			'context' => 'primary',
			'label' => 'Send Reset Link')
			);?>

	<?php $this->endWidget(); ?>
</div>