<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'user-preferences-form',
	'enableAjaxValidation'=>true,
)); ?>


	<?php echo $form->textFieldGroup($model,'email',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>
	
	Update me about:
	<?php echo $form->checkBoxGroup($model,'games',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5', 'checked'=>true)))); ?>
	<?php echo $form->checkBoxGroup($model,'tutorials',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5', 'checked'=>true)))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
