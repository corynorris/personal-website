<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('username')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->username), $this->createUrl('profile/view',array('id'=>$data->id)) );; ?>
	<br />
        
	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo Yii::app()->dateFormatter->formatDateTime($data->createtime, "short"); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastvisit_at')); ?>:</b>
		<?php echo Yii::app()->dateFormatter->formatDateTime($data->lastvisit, "short"); ?>
	<br /><hr />

</div>