<?php
$this->pageTitle= Yii::app()->name . ' - ' . Yii::t('app','Users');

Yii::app()->clientScript->registerScript('search', "
		$('.search-form form').submit(function(){
		$.fn.yiiGridView.update('user-grid', {
		data: $(this).serialize()
});
		return false;
});
		");
?>

<h1>Manage Users</h1>

<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>,
	<b>&gt;</b>, <b>&gt;=</b>, <b> &lt;&gt;</b> or <b>=</b>) at the
	beginning of each of your search values to specify how the comparison
	should be done.
</p>

Advanced Search:
<?php $this->widget(
		'booster.widgets.TbSwitch',
		array(
				'name' => 'testToggleButton',

				'events' => array(
						'switchChange' => 'js:function($el, status, e){$(\'.search-form\').toggle();}'
				),
				'options' => array(
						'size' => 'small', //null, 'mini', 'small', 'normal', 'large
				),
		)
    );?>
<div class="search-form" style="display: none">
	<?php $this->renderPartial('_search',array(
			'model'=>$model,
)); ?>
</div>
<!-- search-form -->

<?php $this->widget('booster.widgets.TbExtendedGridView',array(
		'id'=>'user-grid',
		'fixedHeader' => true,
		'headerOffset' => 40,
		'type' => 'striped',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'template' => "{items}",
		'responsiveTable' => true,
		'columns'=>array(
				'id',
				'username',
				'email',
				array(
						'name'=>'create_at',
						'value' => 'Yii::app()->format->date($data->create_at);',
				),
				array(
						'name'=>'lastvisit_at',
						'value' => 'Yii::app()->format->date($data->lastvisit_at);',
				),
				'superuser',

				array(
						'class'=>'booster.widgets.TbButtonColumn',
				),
		),
)); ?>
