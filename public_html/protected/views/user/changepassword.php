<?php
$this->pageTitle = Yii::app()->name . ' - ' . Helper::t("Change Password");
$this->breadcrumbs = array(
    Helper::t("Profile") => array('/user/profile'),
    Helper::t("Change Password"),
);
?>
<div class="jumbotron masthead">
    <div class="container">
        <h1><?php echo Helper::t("Change password"); ?></h1>
    </div>
</div>
<div class="container">
    <div class="row">

        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'changepassword-form',
            'htmlOptions' => array('class' => 'well'),
        ));
        ?>
        <p><?php echo Helper::t('Fields with <span class="required">*</span> are required.'); ?></p>

        <?php echo $form->errorSummary(array($model)); ?>
        <?php echo $form->passwordFieldControlGroup($model, 'oldPassword', array('class' => 'span3')); ?>

        <?php echo $form->passwordFieldControlGroup($model, 'password', array('class' => 'span3', 'hint' => Helper::t("Minimal password length 4 symbols."))); ?>
        <?php echo $form->passwordFieldControlGroup($model, 'verifyPassword', array('class' => 'span3')); ?>

        <div class="row">
            <div class="col-xs-12 text-right">
                <button type="submit" class="btn btn-default btn-lg">
                    Change Password<span class="glyphicon glyphicon-arrow-right"></span> 
                </button>            
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>