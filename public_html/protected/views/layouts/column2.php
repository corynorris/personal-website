<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="container">
	<div class="row">
		<div class="col-sm-9">

			<?php echo $content; ?>
		</div>
		<div class="col-sm-3">
			<br>
			<?php if(!Yii::app()->user->isGuest) $this->widget('AdminMenu'); ?>

			<?php $this->widget('TagCloud', array(
					'maxTags'=>Yii::app()->params['tagCloudCount'],
  			  )); ?>
			<?php $this->widget('UserMenu'); ?>
		</div>
	</div>
</div>
<!-- content -->
<?php $this->endContent(); ?>
