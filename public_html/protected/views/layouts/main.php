<?php 
// Get client script
$cs=Yii::app()->clientScript;
$baseUrl = Yii::app()->baseUrl;

// Add CSS
$cs->registerCSSFile($baseUrl.'/css/main.css');

// Add Jquery
$cs->registerCoreScript('jquery');

// Add lightGallery
$cs->registerCSSFile($baseUrl.'/jquery/lightGallery/css/lightGallery.css');
$cs->registerScriptFile($baseUrl.'/jquery/lightGallery/js/lightGallery.min.js');

// Add Jquery share
$cs->registerCSSFile($baseUrl.'/jquery/share/jquery.share.css');
$cs->registerScriptFile($baseUrl.'/jquery/share/jquery.share.js');


// Add Syntax Highlighter
$cs->registerCssFile($baseUrl.'/jquery/syntaxHighlighter/styles/shCore.css');
$cs->registerCssFile($baseUrl.'/jquery/syntaxHighlighter/styles/shThemeDefault.css');
$cs->registerScriptFile($baseUrl.'/jquery/syntaxHighlighter/scripts/shCore.js');
$cs->registerScriptFile($baseUrl.'/jquery/syntaxHighlighter/scripts/shAutoloader.js');

?>

<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="language" content="en" />

<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>



<?php $this->widget('booster.widgets.TbNavbar', array(
		'brand' => 'LHG',
		'brandOptions' => array('style' => 'width:auto;margin-left: 0px;'),
		'fixed' => 'top',
		'fluid' => false,
		'collapse' => true,
		'items' => array(
				array(
						'class' => 'booster.widgets.TbMenu',
						'type' => 'navbar',
						'items' => array(
								array('label' => 'Wobble Rocket', 'url' => array('site/wobbleRocket')),
								array('label' => 'Ant Simulator', 'url' => array('site/antSimulator')),
								array('label' => 'Dev Blog & Tutorials', 'url' => array('post/index')),
								array('label' => 'About', 'url'=>array('site/page', 'view'=>'about')),
								array('label' => 'Contact', 'url'=> array('site/contact')),
								array('label' => 'Logout', 'url'=>array('auth/logout'), 'visible'=>!Yii::app()->user->isGuest)
						),
				),
		),
)); ?>



<body>
	<?php echo $content; ?>
	<div id="share"></div>
</body>
<script type="text/javascript">

	
	$( document ).ready(function() {
		function path()
      {
        var args = arguments,
            result = []
            ;
             
        for(var i = 0; i < args.length; i++)
            result.push(args[i].replace('@', '<?php echo $baseUrl;?>/jquery/syntaxHighlighter/scripts/'));
             
        return result
      };
	       
	      SyntaxHighlighter.autoloader.apply(null, path(
	        'applescript            @shBrushAppleScript.js',
	        'actionscript3 as3      @shBrushAS3.js',
	        'bash shell             @shBrushBash.js',
	        'coldfusion cf          @shBrushColdFusion.js',
	        'cpp c                  @shBrushCpp.js',
	        'c# c-sharp csharp      @shBrushCSharp.js',
	        'css                    @shBrushCss.js',
	        'delphi pascal          @shBrushDelphi.js',
	        'diff patch pas         @shBrushDiff.js',
	        'erl erlang             @shBrushErlang.js',
	        'groovy                 @shBrushGroovy.js',
	        'java                   @shBrushJava.js',
	        'jfx javafx             @shBrushJavaFX.js',
	        'js jscript javascript  @shBrushJScript.js',
	        'perl pl                @shBrushPerl.js',
	        'php                    @shBrushPhp.js',
	        'text plain             @shBrushPlain.js',
	        'py python              @shBrushPython.js',
	        'ruby rails ror rb      @shBrushRuby.js',
	        'sass scss              @shBrushSass.js',
	        'scala                  @shBrushScala.js',
	        'sql                    @shBrushSql.js',
	        'vb vbnet               @shBrushVb.js',
	        'xml xhtml xslt html    @shBrushXml.js'
	    ));
		SyntaxHighlighter.all();
		
		$("#lightGallery").lightGallery({speed:200,});
		    	   
		$('#share').share({
			networks: ['twitter','facebook','tumblr','pinterest','googleplus'],
			orientation: 'vertical',
			urlToShare: 'http://www.lasthackergames.com',
			affix: 'right center'
		});
	});

    	    
    	 
</script>
</html>
