<?php
/* @var $this PostController */
/* @var $model Post */
$this->pageTitle= Yii::app()->name . ' - ' . Yii::t('app',$model->title);
?>

<h1>
	Update Post
	<?php echo $model->id; ?>
</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>