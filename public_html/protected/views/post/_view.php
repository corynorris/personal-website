<?php
/* @var $this PostController */
/* @var $data Post */
?>

<div class="post">
	<div class="title">
		<h1>
			<?php echo CHtml::link(CHtml::encode(CHtml::encode($data->title)),$data->url);  ?>
		</h1>
		<?php // Render them all with single `TbAlert`
$this->widget('booster.widgets.TbAlert', array(
		'fade' => true,
		'closeText' => '&times;', // false equals no close link
		'userComponentId' => 'user',
		'alerts' => array( // configurations per alert type
				'success','danger', // you don't need to specify full config
		),
		));?>

	</div>
	<div class="author">
		<strong> posted by <?php echo $data->author->username . ' on ' . date('F j, Y',$data->create_time); ?>
		</strong>
	</div>


	<?php
	$this->beginWidget('CMarkdown', array('purifyOutput'=>true));
	echo $data->content;
	$this->endWidget();
	?>

	<div class="nav">
		<b>Tags:</b>
		<?php echo implode(', ', $data->tagLinks); ?>
		<br />
		<?php echo CHtml::link('Permalink', $data->url); ?>
		|
		<?php echo CHtml::link("Comments ({$data->commentCount})",$data->url.'#comments'); ?>
		| Last updated on
		<?php echo date('F j, Y',$data->update_time); ?>
	</div>


</div>
