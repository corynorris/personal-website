<?php
/* @var $this PostController */
/* @var $dataProvider CActiveDataProvider */
$this->pageTitle=$this->pageTitle= Yii::app()->name . ' - ' . 'Dev Blog & Tutorials';
?>

<?php if(!empty($_GET['tag'])): ?>
<h1>
	Posts Tagged with <em>'<?php echo CHtml::encode($_GET['tag']); ?>'
	</em>
</h1>
<?php endif; ?>

<?php $this->widget('booster.widgets.TbListView',array(
		'dataProvider'=>$dataProvider,
		'itemView'=>'_view',
		'template'=>"{items}\n{pager}",
));  ?>



