<?php
/* @var $this PostController */
/* @var $model Post */
$this->pageTitle= Yii::app()->name . ' - ' . Yii::t('app','Manage Posts');
Yii::app()->clientScript->registerScript('search', "
		$('.search-form form').submit(function(){
		$('#post-grid').yiiGridView('update', {
		data: $(this).serialize()
});
		return false;
});
		");
?>

<h1>Manage Posts</h1>

<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>,
	<b>&gt;</b>, <b>&gt;=</b>, <b> &lt;&gt;</b> or <b>=</b>) at the
	beginning of each of your search values to specify how the comparison
	should be done.
</p>

Advanced Search:
<?php $this->widget(
		'booster.widgets.TbSwitch',
		array(
				'name' => 'testToggleButton',

				'events' => array(
						'switchChange' => 'js:function($el, status, e){$(\'.search-form\').toggle();}'
				),
				'options' => array(
						'size' => 'small', //null, 'mini', 'small', 'normal', 'large
				),
		)
    );?>
<div class="search-form" style="display: none">
	<?php $this->renderPartial('_search',array(
			'model'=>$model,
)); ?>
</div>
<!-- search-form -->

<?php $this->widget('booster.widgets.TbExtendedGridView',array(
		'id'=>'post-grid',
		'fixedHeader' => true,
		'headerOffset' => 40,
		'type' => 'striped',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'template' => "{items}",
		'responsiveTable' => true,
		'columns'=>array(
				'id',
				'title',
				array(
						'name'=>'content',
						'value' => 'CHtml::encode(Helper::truncateText($data->content,Yii::app()->params[\'truncationLength\']));',
				),
					
				'tags',
				'status',
				array(
						'name'=>'create_time',
						'value' => 'Yii::app()->format->date($data->create_time);',
				),
				array(
						'name'=>'update_time',
						'value' => 'Yii::app()->format->date($data->update_time);',
				),

				array(
						'htmlOptions' => array('nowrap'=>'nowrap'),
						'class'=>'booster.widgets.TbButtonColumn',
						'template'=>'{view}{update}{delete}',
				),
				/*
				 'author_id',
*/

		),
)); ?>