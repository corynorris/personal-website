<?php
/* @var $this PostController */
/* @var $model Post */
/* @var $form TbActiveForm */
?>
<script>
	function openKCFinder_singleFile() {
	    window.KCFinder = {};
	    window.KCFinder.callBack = function(url) {
	        // Actions with url parameter here
	        window.KCFinder = null;
	        $('#thumbUrl').val(url);
	    };
	    window.open('<?php echo Yii::app()->baseUrl . '/vendor/kcfinder/browse.php?type=images'; ?>');
	};
	
	 $(document).ready(
			 function() {
				 $('#getUrl').click(function() {
					openKCFinder_singleFile();
					//alert('getting url');
				});
			});
</script>
<div class="form">
	<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
			'id'=>'post-form',
			// Please note: When you enable ajax validation, make sure the corresponding
			// controller action is handling ajax validation correctly.
			// There is a call to performAjaxValidation() commented in generated controller code.
			// See class documentation of CActiveForm for details on this.
			'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">
		Fields with <span class="required">*</span> are required.
	</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'title',array('span'=>5,'maxlength'=>128)); ?>

	<?php echo $form->textAreaGroup($model, 'summary',array('rows'=>6,'span'=>6)); ?>
	
	<?php 
	echo '<div class="form-group">';
	echo $form->label($model,'thumb_url', array('required'=>true));
	echo '<div class="input-group">';
	echo $form->textField(
			$model,
			'thumb_url',
			array(
					'id'=>'thumbUrl',
					'class'=>'form-control'
			)
	);
	echo '<div class="input-group-btn">';
	$this->widget(
			'booster.widgets.TbButton',
			array(
					'label' => 'Set File',
					'id'=> 'getUrl',
			)
	);
	echo '</div>';
	echo '</div>';
	echo '</div>';
	?>

	<?php echo $form->ckEditorGroup(
			$model,
			'content',
			array(
					'wrapperHtmlOptions' => array(
							/* 'class' => 'col-sm-5', */
					),
					'widgetOptions' => array(
							'editorOptions' => array(
									'filebrowserImageBrowseUrl'=> Yii::app()->baseUrl . '/vendor/kcfinder/browse.php?type=images',
									'fullpage' => 'js:true',
									/* 'width' => '640', */
									/* 'resize_maxWidth' => '640', */
									/* 'resize_minWidth' => '320'*/
							)
					)
			)
); ?>
	<?php 
	/*
	 echo $form->redactorGroup(
	 		$model,
	 		'content',
	 		array(
	 				'widgetOptions' => array(
	 						'editorOptions' => array(
	 								'fileUpload' => $this->createUrl('site/testFileUpload'),
	 								'imageUpload' => $this->createUrl('site/testImageUpload'),
	 								//									'imageGetJson'=> $this->createUrl('site/testImageThumbs'),
	 								'width'=>'100%',
	 								'height'=>'400px',
	 								'options' => array('color' => true)
	 						),
	 				),

	 		)
			); */?>


	<?php echo $form->textAreaGroup($model,'tags',array('rows'=>6,'span'=>6)); ?>


	<?php echo $form->dropDownListGroup(
			$model,
			'status',
			array(
					'wrapperHtmlOptions' => array(
							'class' => 'col-sm-5',
					),
					'widgetOptions' => array(
							'data' => Lookup::items('PostStatus'),
							'htmlOptions' => array('multiple' => false),
					)
			)
); ?>

	<?php $this->widget(
			'booster.widgets.TbButton',
			array('buttonType' => 'reset', 'label' => 'Reset')
		); ?>
	<?php $this->widget( 'booster.widgets.TbButton',array(
			'buttonType' =>	'submit',
			'context' => 'primary',
			'label' => $model->isNewRecord ? 'Create' : 'Save')
			);?>

	<?php $this->endWidget(); ?>

</div>
<!-- form -->
