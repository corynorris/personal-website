<?php
/* @var $this PostController */
/* @var $model Post */
$this->pageTitle= Yii::app()->name . ' - ' . Yii::t('app','Create Post');
?>

<h1>Create Post</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>