<?php
/* @var $this PostController */
/* @var $model Post */
/* @var $form CActiveForm */
?>

<div class="wide form">

	<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
			'action'=>Yii::app()->createUrl($this->route),
			'method'=>'get',
)); ?>

	<?php echo $form->textFieldGroup($model,'id',array('span'=>5)); ?>

	<?php echo $form->textFieldGroup($model,'title',array('span'=>5,'maxlength'=>128)); ?>

	<?php echo $form->textAreaGroup($model,'content',array('rows'=>6,'span'=>8)); ?>

	<?php echo $form->textAreaGroup($model,'tags',array('rows'=>6,'span'=>8)); ?>

	<?php echo $form->textFieldGroup($model,'status',array('span'=>5)); ?>

	<?php echo $form->textFieldGroup($model,'create_time',array('span'=>5)); ?>

	<?php echo $form->textFieldGroup($model,'update_time',array('span'=>5)); ?>

	<?php echo $form->textFieldGroup($model,'author_id',array('span'=>5)); ?>

	<div class="form-actions">
		<?php $this->widget( 'booster.widgets.TbButton',array(
				'buttonType' =>	'submit',
				'context' => 'primary',
				'label' => 'Search')
			);?>
	</div>

	<?php $this->endWidget(); ?>

</div>
<!-- search-form -->
