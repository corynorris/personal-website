<?php 

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
//$cs->registerScriptFile("https://ssl-webplayer.unity3d.com/download_webplayer-3.x/3.0/uo/jquery.min.js");
$cs->registerScriptFile($baseUrl.'/js/spaceShooter.js');
$cs->registerCssFile($baseUrl.'/css/Unity3d.css');

?>
<div class="container">
	<div class="page-header">
		<h1>
			<?php echo Yii::t('app', "Space Shooter"); ?>
			<small>An implementation of Unity's tutorial game: space shooter</small>
		</h1>
	</div>
	<div class="content">
		<div id="unityPlayer">
			<div class="missing">
				<a href="http://unity3d.com/webplayer/" title="Unity Web Player. Install now!">
					<img alt="Unity Web Player. Install now!" src="http://webplayer.unity3d.com/installation/getunity.png" width="193" height="63" />
				</a>
			</div>
			<div class="broken">
				<a href="http://unity3d.com/webplayer/" title="Unity Web Player. Install now! Restart your browser after install.">
					<img alt="Unity Web Player. Install now! Restart your browser after install." src="http://webplayer.unity3d.com/installation/getunityrestart.png" width="193" height="63" />
				</a>
			</div>
		</div>
	</div>
</div>

