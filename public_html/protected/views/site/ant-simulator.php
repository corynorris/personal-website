<?php 
$this->pageTitle= Yii::app()->name . ' - ' . Yii::t('app','Ant Simulator');
$quotedUrl = CJavaScript::encode($this->createUrl('/site/disableAlert'));

Yii::app()->clientScript->registerScript('close', "
		$('#info').on('closed.bs.alert',
		function() {".

		CHtml::ajax(array(
				'url'=>array('/site/disableAlert'),
				'data'=>array('showAlert'=>false),
				'type'=>'POST')).
		"});"
);


?>
<div class="container">

	<div class="page-header">
		<h1>
			<?php echo Yii::t('app', "Ant Simulator"); ?>
			<small>a battle of colonies</small>
		</h1>
		<?php // Render them all with single `TbAlert`
$this->widget('booster.widgets.TbAlert', array(
		'fade' => true,
		'id'=>'info',
		'closeText' => '&times;', // false equals no close link
		'userComponentId' => 'user',
		'alerts' => array( // configurations per alert type
				'info', // you don't need to specify full config

		),
		));?>

		<?php	
		$baseUrl = Yii::app()->baseUrl;
		echo CHtml::image($baseUrl.'/images/banner/ant_banner.jpg','rocket graphic from game',array('class'=>'img-rounded', 'width'=>'100%')); ?>
	</div>





	<div class="row">
		<div class="col-sm-8">
			<h2>Description</h2>
			<p>Ant simulator puts you in full control of an ant colony. Through
				the control of your hero ant, as well as macro hive factors (such as
				the type of ants to spawn) you must defeat the red ant colony</p>




		</div>
		<div class="col-sm-4">
			<h2>Planned Features</h2>
			<ul>
				<li>Unlock spider mode!</li>
				<li>Randomly generated maps!</li>
				<li>Full tutorial!</li>
			</ul>

		</div>
	</div>
	<div class="row">
		<div class="col-sm-8 clearfix">
			<h2>Media</h2>

			<?php
			$baseUrl = Yii::app()->baseUrl . '/images/ant-simulator/';
			$links = array(
					array('data-src'=>$baseUrl.'ants-background.jpg',
							'src'=>$baseUrl.'ants-background.jpg',
							'alt'=>'Artistic background'),
					array('data-src'=>$baseUrl.'ants-fighting.jpg',
							'src'=>$baseUrl.'ants-fighting.jpg',
							'alt'=>'Two ant colonies fighting'),
					array('data-src'=>$baseUrl.'ants-walking.jpg',
							'src'=>$baseUrl.'ants-walking.jpg',
							'alt'=>'Ants searching for food'),
					array('data-src'=>'http://youtu.be/0tVuDASTJyY',
							'src'=>'http://img.youtube.com/vi/0tVuDASTJyY/0.jpg',
							'alt'=>'artistic background'),

);?>
			<ul id="lightGallery" class="gallery list-unstyled">

				<?php
				foreach ($links as $link) {

					echo "<li data-src=\"".$link['data-src']."\"><img src=\"".$link['src']."\" alt=\"".$link['alt']."\"	width=\"100\" height=\"100\" /></li>";
				}
				?>
			</ul>
		</div>
		<div class="col-sm-4">
			<h2>Get the App!</h2>
			Not yet available
		</div>

	</div>
</div>
