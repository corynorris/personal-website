<div class="container">
	<div class="page-header">
		<h1>
			<?php echo Yii::t('app', "Wobble Rocket"); ?>
			<small>high-speed rhythmic evasion</small>
		</h1>
		<?php
		$baseUrl = Yii::app()->baseUrl;
		echo CHtml::image($baseUrl.'/images/banner/rocket_banner.jpg','rocket graphic from game',array('class'=>'img-rounded', 'width'=>'100%')); ?>
	</div>
	<div class="row">
		<div class="col-sm-8">
			<h2>Description</h2>
			<p>Wobble Rocket is a simple game that lets you control a rocket as
				it falls towards the ground. The goal of the game is to guide a
				rocket left to right - dodging obstacles. Though seemingly simple,
				Wobble Rocket is more difficult than it appears, requiring a
				rhythmic coordination of movements.</p>

		</div>
		<div class="col-sm-4">
			<h2>Features</h2>
			<ul>
				<li>5 Unlock-able rockets!</li>
				<li>Progressive difficulty!</li>
			</ul>

		</div>
	</div>
	<div class="row">
		<div class="col-sm-8 clearfix">
			<h2>Media</h2>
			<?php 
			$wobbleRocket = Yii::app()->baseUrl . '/images/wobble-rocket/';
			$links = array(
					array('data-src'=>$wobbleRocket.'gameplay1.jpg',
							'src'=>$wobbleRocket.'gameplay1.jpg',
							'alt'=>'Advanced rocket gameplay'),
					array('data-src'=>$wobbleRocket.'gameplay2.jpg',
							'src'=>$wobbleRocket.'gameplay2.jpg',
							'alt'=>'Basic rocket gameplay'),
					array('data-src'=>$wobbleRocket.'icon.jpg',
							'src'=>$wobbleRocket.'icon.jpg',
							'alt'=>'Application icon'),
					array('data-src'=>$wobbleRocket.'menu.jpg',
							'src'=>$wobbleRocket.'menu.jpg',
							'alt'=>'Initial game menu'),

			);?>

			<ul id="lightGallery" class="gallery list-unstyled">

				<?php 
				foreach ($links as $link) {
					echo "<li data-src=\"".$link['data-src']."\"><img src=\"".$link['src']."\" alt=\"".$link['alt']."\"	width=\"100\" height=\"100\" /></li>";
				}
				?>
			</ul>
		</div>
		<div class="col-sm-4">
			<h2>Get the App!</h2>
			<a
				href="https://play.google.com/store/apps/details?id=com.LastHackerGames.WobbleRocket">
				<?php echo CHtml::image($baseUrl.'/images/brand/en_generic_rgb_wo_60.jpg','Get it on Google Play',array('class'=>'img-rounded')); ?>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<h2>Play Online!</h2>
			<h2>Controls</h2>
			<p>Tap left of the rocket to accelerate left. Top right of the rocket
				to accelerate right.</p>

		</div>
	</div>

</div>
