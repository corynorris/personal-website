<?php
$baseUrl = Yii::app()->baseUrl;
?>
<div
	id="carousel-example-generic" class="carousel slide"
	data-ride="carousel">

	<ol class="carousel-indicators">
		<li data-target="#carousel-example-generic" data-slide-to="0"
			class="active"></li>
		<li data-target="#carousel-example-generic" data-slide-to="1"></li>
	</ol>

	<!-- Wrapper for slides -->
	<div class="carousel-inner">
		<div class="item active">
			<?php echo CHtml::image($baseUrl.'/images/banner/ant_banner.jpg','rocket graphic from game',array('width'=>'100%')) ?>
			<div class="carousel-caption">
				<h2>Ant Simulator</h2>
				<p>A work in progress game, where-in you battle 2 ant colonies!</p>
			</div>
		</div>
		<div class="item">
			<?php echo CHtml::image($baseUrl.'/images/banner/rocket_banner.jpg','rocket graphic from game',array('width'=>'100%')) ?>
			<div class="carousel-caption">
				<h2>Wobble Rocket</h2>
				<p>My first title demonstrating some of LibGdx's capabilities.</p>
			</div>
		</div>

	</div>
	<!-- Controls -->
	<a class="left carousel-control" href="#carousel-example-generic"
		role="button" data-slide="prev"> <span
		class="glyphicon glyphicon-chevron-left"></span>
	</a> <a class="right carousel-control" href="#carousel-example-generic"
		role="button" data-slide="next"> <span
		class="glyphicon glyphicon-chevron-right"></span>
	</a>
</div>
<div class="container">
	<div class="page-header">
		<h1>
			<?php echo Yii::t('app', "Everything Recent"); ?>
			<small>posts, comments, etc</small>
		</h1>
		<?php // Render them all with single `TbAlert`
$this->widget('booster.widgets.TbAlert', array(
		'fade' => true,
		'closeText' => '&times;', // false equals no close link
		'userComponentId' => 'user',
		'alerts' => array( // configurations per alert type
				'success','danger', // you don't need to specify full config
		),
		));?>
	</div>
	<div class="row">
		<div class="col-sm-8">

			<?php $this->widget('RecentPosts', array(
					'maxPosts'=>Yii::app()->params['recentPostCount'],
					'truncationLength'=>Yii::app()->params['truncationLength'],
							));?>



		</div>
		<div class="col-sm-4">
			<?php $this->beginWidget(
					'booster.widgets.TbPanel',
					array(
							'title' => 'Get Updates',
							'headerIcon' => 'bullhorn',
							'context'=>'primary'

					)
			); ?>
			<?php if (isset(Yii::app()->session['subscribed']) && (Yii::app()->session['subscribed'] == true)) {
				echo 'Thanks for subscribing!';					
			} else
			{
				$this->renderPartial('/userPreferences/_subscribe',array(
						'model'=>$model,
				));
			}
			?>
			<?php $this->endWidget(); ?>

			<?php $this->widget('RecentComments', array(
					'maxComments'=>Yii::app()->params['recentCommentCount'],
   			 )); ?>


		</div>
	</div>
</div>
