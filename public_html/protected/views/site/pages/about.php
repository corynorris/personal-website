<?php
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('app', "About");
$baseUrl = Yii::app()->baseUrl;

// & register scripts

//$cs=Yii::app()->clientScript;

// Add Jquery share
//$cs->registerCSSFile($baseUrl.'/jquery/panorama/panorama_viewer.css');
//$cs->registerScriptFile($baseUrl.'/jquery/panorama/jquery.panorama_viewer.min.js');
?>
<div class="container">
	<div class="page-header">
		<h1>
			<?php echo Yii::t('app', "About"); ?>
			<small>me, the website, and lhg</small>
		</h1>
	</div>
	<div class="row">
		<div class="col-sm-12 col-md-10 col-lg-8">
			<div class="media">
				<a class="pull-left" href="#"><?php echo CHtml::image($baseUrl.'/images/about/seoul350.jpg','Me, standing in front of Seouls imperial temple.', array('class'=>'media-object img-thumbnail')); ?>
				</a>
				<div class="media-body">
					<h2 class="media-heading">Who am I?</h2>
					<p>
						My name is Cory, and I recently graduated from Wilfrid Laurier. I
						have an interest in computer science, and am particularly
						interested in topics of graphics, video game design, machine
						learning and virtual & augmented reality. I often forget, but when
						I remember to upload things, much of my work can be found on <a
							href="https://bitbucket.org/cnorris">bitbucket</a>
					</p>
					<p>Outside of academics and computer science, I also enjoy
						travelling and movies. The photos featured on this about page come
						from some of my travels to south east asia.</p>
				</div>
			</div>
			<div class="media">
				<a class="pull-right" href="#"><?php echo CHtml::image($baseUrl.'/images/about/china350.jpg','A small city near Zhangjiajie, China.', array('class'=>'media-object img-thumbnail')); ?>
				</a>

				<div class="media-body">
					<h2 class="media-heading">What is LHG?</h2>
					<p>LHG is the archive and landing page to some of my work. My
						current goal is to become more capable at designing and
						implementing video games, and I suspect that it's a goal shared by
						many others. way.</p>
					<p>LHG also serves as a show case for the games I have - and will -
						create. By showcasing my games, I hope to get feedback and
						criticism so that I can ultimately create better games.</p>

					<h2>Goals of this website:</h2>
					<p>Though subject to change, I currently have two goals with this
						website.</p>
					<ul>
						<li>First, I'd like to get feedback and criticism so that I can
							build better games.</li>
						<li>Secondly, by documenting all, or even some of my learning
							process and sharing it online, I hope to develop the development
							community in some small way</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
