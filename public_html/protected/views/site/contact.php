<?php
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('app', "Contact");
?>


<div class="container">
	<div class="page-header">
		<h1>
			<?php echo Yii::t('app', "Contact Me"); ?>
			<small>for anything and everything</small>
		</h1>
		
		<p>If you need any help using the site, would like to request a
			feature, or really any reason, please contact me.</p>
					<?php // Render them all with single `TbAlert`
$this->widget('booster.widgets.TbAlert', array(
		'fade' => true,
		'id'=>'info',
		'closeText' => '&times;', // false equals no close link
		'userComponentId' => 'user',
		'alerts' => array( // configurations per alert type
				'success', // you don't need to specify full config

		),
		));?>
	</div>

	<?php
	$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
			'id' => 'contact-form',
			'htmlOptions' => array('class' => 'well'),
	));
	?>

	<p>
		<?php echo Helper::t('Fields with <span class="required">*</span> are required.'); ?>
	</p>
	<?php echo $form->errorSummary($model); ?>
	<?php echo $form->textFieldGroup($model, 'name', array('autocomplete' => 'off')); ?>
	<?php echo $form->textFieldGroup($model, 'email', array('autocomplete' => 'off')); ?>
	<?php echo $form->textFieldGroup($model, 'subject', array('autocomplete' => 'off')); ?>
	<?php echo $form->textAreaGroup($model, 'body', array('autocomplete' => 'off')); ?>
	<?php
	echo '<div class="form-group">';
	echo $form->label($model, 'verifyCode', array('required'=>true, 'class'=>'control-label'));
	echo '<br>';
	echo $this->widget('system.web.widgets.captcha.CCaptcha', array(), true);
	echo '<br>';
	echo $form->textField($model, 'verifyCode', array('class'=>'form-control', 'placeholder'=>'Verification Code'));

	echo '</div>';
	?>

	<?php $this->widget( 'booster.widgets.TbButton',array(
			'buttonType' =>	'submit',
			'context' => 'primary',
			'label' => 'Send')
			);?>
	<?php $this->endWidget(); ?>
</div>

