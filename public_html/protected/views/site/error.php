<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle = Yii::app()->name . ' - Error';
$this->breadcrumbs = array(
    'Error',
);
?>
<div class="jumbotron masthead">
    <div class="container">
        <h1><h2>Error <?php echo $code; ?></h2></h1>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="error">
                <?php echo CHtml::encode($message); ?>
            </div>
        </div>
    </div>
</div>
