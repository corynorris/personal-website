<?php

Yii::import('zii.widgets.CPortlet');

class UserMenu extends CPortlet
{
	public function init()
	{
		parent::init();
	}

	public function getPosts()
	{
		return Post::model()->published()->recently()->findAll();
	}

	protected function renderContent()
	{
		$this->render('userMenu');
	}
}