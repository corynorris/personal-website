<?php $this->beginWidget(
		'booster.widgets.TbPanel',
		array(
				'title' => 'Older Posts',
				'headerIcon' => 'list',
				'context'=>'primary'


		)
			); ?>

<?php 
$menu = array();
?>
<?php foreach($this->getPosts() as $post): ?>
<?php array_push($menu,array('label' => $post->title,'url' =>$post->getUrl())); ?>
<?php endforeach; ?>
<?php 
$this->widget('booster.widgets.TbMenu', array( 'type' => 'pills',
	'stacked' => true, 'items' => $menu, )); ?>
<?php $this->endWidget(); ?>
