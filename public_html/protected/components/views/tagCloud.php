<?php $this->beginWidget(
		'booster.widgets.TbPanel',
		array(
				'title' => 'Tag Filter',
				'headerIcon' => 'tags',
				'context'=>'primary'
				

		)
			); ?>
<?php $tags=Tag::model()->findTagWeights($this->maxTags);

foreach($tags as $tag=>$weight)
{
	$link=CHtml::link(CHtml::encode($tag), array('post/index','tag'=>$tag));
	echo CHtml::tag('span', array(
			'class'=>'tag',
			'style'=>"font-size:{$weight}pt",
			), $link)."\n";
} ?>
<?php $this->endWidget(); ?>