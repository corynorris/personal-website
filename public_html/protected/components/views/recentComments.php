<?php $this->beginWidget(
		'booster.widgets.TbPanel',
		array(
				'title' => 'Recent Comments',
				'headerIcon' => 'comment',
				'context'=>'primary'


		)
			); ?>
<?php foreach($this->getRecentComments() as $comment): ?>

<div class="row clearfix">
	<div class="col-md-12">
		<strong><em>"<?php echo $comment->authorLink; ?>"</em></strong> commented on
		<?php echo CHtml::link(CHtml::encode($comment->post->title), $comment->getUrl()); ?>

	</div>
</div>

<?php endforeach; ?>
<?php $this->endWidget(); ?>