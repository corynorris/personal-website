<div class="clearfix">
<?php $this->beginWidget(
		'booster.widgets.TbPanel',
		array(
				'title' => 'Admin Options',
				'headerIcon' => 'wrench',
				'context'=>'primary'


		)
			); ?>

	<?php 
	$menu = array(
			array('label' => 'List Posts','url' =>array('/post/index')),
			array('label' => 'Create New Post','url' =>array('/post/create')),
			array('label' => 'Manage Posts','url' =>array('/post/admin')),
			array('label' => 'Approve Comments' . ' (' . Comment::model()->pendingCommentCount . ')','url' =>array('/comment/index')),
			array('label' => 'List Users','url' =>array('/user/index')),
			array('label' => 'Create New User','url' =>array('/user/create')),
			array('label' => 'Manage Users','url' =>array('/user/admin')),

	);

	$this->widget('booster.widgets.TbMenu', array(
			'type' => 'pills',
			'stacked' => true,
			'items' => $menu,
	)); ?>
<?php $this->endWidget(); ?>
</div>
