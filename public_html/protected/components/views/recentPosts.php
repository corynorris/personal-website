
<div class="post-summary">
	<?php foreach($this->getRecentPosts() as $post): ?>
	<div class="row">
		<div class="col-md-12">
			<div class="media">
				<a class="pull-left" href="<?php echo $post->getUrl(); ?>"> <?php  echo CHtml::image($post->thumb_url, $post->title, array('width'=>'125px', 'height'=>'125px')); ?>
				</a>
				<h1>
					<?php echo CHtml::encode($post->title); ?>
				</h1>
				<p>
					<?php echo $post->summary ?>
					<?php /*echo Helper::truncateText(CHtml::encode($post->content),
					$this->truncationLength); */ ?>
				</p>
			</div>


		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php echo '<strong>Tags: </strong>';
			foreach ($post->getTags() as $tag=>$url)
			{

				$this->widget(
						'booster.widgets.TbLabel',
						array(
								'context' => 'default',
								// 'default', 'primary', 'success', 'info', 'warning', 'danger'
								'label' => $tag,
						)
				);
				echo ' ';

			}
			?>
			<div class="pull-right">
				<a href="<?php echo $post->getUrl(); ?>"> <?php 		$this->widget(
						'booster.widgets.TbButton',
						array(
								'icon' => 'chevron-right',
								'label' => 'Read Full Article',
								'size' => 'extra_small',
								'url'=> $post->getUrl()
						)
				); ?> <?php  ?>
				</a>

			</div>
		</div>

	</div>

	<hr>
	<?php endforeach; ?>
</div>
