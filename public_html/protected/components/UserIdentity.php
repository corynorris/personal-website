<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    private $_id;

    const ERROR_USERNAME_OR_PASSWORD_INVALID = 1;
    const ERROR_STATUS_NOTACTIV = 2;
    const ERROR_STATUS_BAN = 3;

        /**
     * Evaluates if the login attempt was successful
     */
    private function attemptLogin($user)
    {
       return  (Helper::decrypting($this->password, $user->password) == $user->password);
    }
    
    /**
     * Authenticates a user.
     * @return boolean whether authentication succeeds.
     */
    public function authenticate() {
    	
    	
        if (strpos($this->username, "@")) {
            $user = User::model()->notsafe()->findByAttributes(array('email' => $this->username));
        } else {
            $user = User::model()->notsafe()->findByAttributes(array('username' => $this->username));
        }
       
		
        
        // Check if the username or email was found
        if ($user === null)
        {      
            $this->errorCode = self::ERROR_USERNAME_OR_PASSWORD_INVALID;
        }
        else if ($user->status == 0 && !Helper::$loginNotActiv)
        {
            // They haven't activated their account yet
            $this->errorCode = self::ERROR_STATUS_NOTACTIV;
        }            
        else if ($user->status == -1)
        {
            // They've been banned
            $this->errorCode = self::ERROR_STATUS_BAN;
        }            
        else if (!$this->attemptLogin($user))
        {
            $this->errorCode = self::ERROR_USERNAME_OR_PASSWORD_INVALID;
        }
        else if ($this->attemptLogin($user))
        {
            $this->_id = $user->id;
            $this->username = $user->username;
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }
    


    /**
     * @return integer the ID of the user record
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * Returns the display name for the identity.
     * The default implementation simply returns {@link username}.
     * This method is required by {@link IUserIdentity}.
     * @return string the display name for the identity.
     */
    public function getName() {
        return $this->username;
    }

}
