<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Helper
 *
 * @author cory
 */
class Helper {

    /**
     * @var int
     * @desc items on page
     */
    public static $user_page_size = 10;

    /**
     * @var int
     * @desc items on page
     */
    public static $fields_page_size = 10;

    /**
     * @var string
     * @desc hash method (md5,sha1 or algo hash function http://www.php.net/manual/en/function.hash.php)
     */
    public static $hash = 'blowfish';

    /**
     * @var boolean
     * @desc use email for activation user account
     */
    public static $sendActivationMail = false;

    /**
     * @var boolean
     * @desc allow auth for is not active user
     */
    public static $loginNotActiv = true;

    /**
     * @var boolean
     * @desc activate user on registration (only $sendActivationMail = false)
     */
    public static $activeAfterRegister = true;

    /**
     * @var boolean
     * @desc login after registration (need loginNotActiv or activeAfterRegister = true)
     */
    public static $autoLogin = true;
    public static $registrationUrl = array("/registration");
    public static $recoveryUrl = array("/auth/recovery");
    public static $loginUrl = array("/auth/login");
    public static $logoutUrl = array("/auth/logout");
    public static $profileUrl = array("/user");
    public static $returnUrl = array("/post/index");
    public static $returnLogoutUrl = array("/auth/login");

    /**
     * @var int
     * @desc Remember Me Time (seconds), defalt = 2592000 (30 days)
     */
    public static $rememberMeTime = 2592000; // 30 days
    public static $fieldsMessage = '';

    /**
     * @var boolean
     */
    public static $captcha = array('registration' => true);

    /**
     * @var boolean
     */
    static private $_user;
    static private $_users = array();
    static private $_userByName = array();
    static private $_admin;
    static private $_admins;

    /**
     * @var array
     * @desc Behaviors for models
     */
    public static $componentBehaviors = array();

    /**
     * @param $str
     * @param $params
     * @param $dic
     * @return string
     */
    public static function t($str = '', $params = array(), $dic = 'user') {
        if (Yii::t("app", $str) == $str) {
            return Yii::t("app." . $dic, $str, $params);
        } else {
            return Yii::t("app", $str, $params);
        }
    }

    /**
     * @return hash string.
     */
    public static function encrypting($string = "") {

        $hash = Helper::$hash;
        if ($hash == 'blowfish') {
            return crypt($string, Randomness::blowfishSalt());
        } else if ($hash == 'md5') {
            return md5($string);
        } else if ($hash == 'sha1') {
            return sha1($string);
        } else {
            return hash($hash, $string);
        }
    }

    /*     * ($password_hash === crypt($form->password, $password_hash))
     * @return hash string.
     */

    public static function decrypting($string = "", $passwordHash = "") { //string = form, password = 
        $hash = Helper::$hash;
        if ($hash == 'blowfish') {
            return crypt($string, $passwordHash);
        } else if ($hash == 'md5') {
            return md5($string);
        } else if ($hash == 'sha1') {
            return sha1($string);
        } else {
            return hash($hash, $string);
        }
    }

    /**
     * @param $place
     * @return boolean 
     */
    public static function doCaptcha($place = '') {
        if (!extension_loaded('gd')) {
            return false;
        }
        if (in_array($place, Yii::app()->getModule('user')->captcha)) {
            return Yii::app()->getModule('user')->captcha[$place];
        }
        return false;
    }

    /**
     * Return admin status.
     * @return boolean
     */
    public static function isAdmin() {
        if (Yii::app()->user->isGuest) {
            return false;
        } else {
            if (!isset(self::$_admin)) {
                if (self::user()->superuser) {
                    self::$_admin = true;
                } else {
                    self::$_admin = false;
                }
            }
            return self::$_admin;
        }
    }

    /**
     * Truncates text to a maximum length
     * @param unknown_type $text
     * @param unknown_type $max_len
     */
    public static function truncateText($text, $max_len)
    {
    	//$len = strlen($text, 'UTF-8');
    	$len = strlen($text);
    	if ($len <= $max_len)
    		return $text;
    	else
    		return substr($text, 0, $max_len - 1) . ' ...';
    		//return substr($text, 0, $max_len - 1, 'UTF-8') . '...';
    }
    
    /**
     * Return admins.
     * @return array syperusers names
     */
    public static function getAdmins() {

        if (!self::$_admins) {
            $admins = User::model()->active()->superuser()->findAll();
            $return_name = array();

            foreach ($admins as $admin) {
                array_push($return_name, $admin->username);
            }

            self::$_admins = ($return_name) ? $return_name : array('');
        }
        return self::$_admins;
    }

    /**
     * Send mail method
     */
    public static function sendMail($email, $subject, $message) {
        $adminEmail = Yii::app()->params['adminEmail'];
        $headers = "MIME-Version: 1.0\r\nFrom: $adminEmail\r\nReply-To: $adminEmail\r\nContent-Type: text/html; charset=utf-8";
        $message = wordwrap($message, 70);
        $message = str_replace("\n.", "\n..", $message);
        return mail($email, '=?UTF-8?B?' . base64_encode($subject) . '?=', $message, $headers);
    }

    /**
     * Return safe user data.
     * @param user id not required
     * @return user object or false
     */
    public static function user($id = 0, $clearCache = false) {
        if (!$id && !Yii::app()->user->isGuest) {
            $id = Yii::app()->user->id;
        }
        if ($id) {
            if (!isset(self::$_users[$id]) || $clearCache) {
                self::$_users[$id] = User::model()->with(array('profile'))->findbyPk($id);
            }
            return self::$_users[$id];
        } else {
            return false;
        }
    }

    public static function getDateTime() {
        return date('Y-m-d H:i:s');
    }

    /**
     * 
     */
    public static function getFirstItem($arr, $key) {
        $first = $arr[0];
        return $first[$key];
    }

    /**
     * Return safe user data.
     * @param user name
     * @return user object or false
     */
    public static function getUserByName($username) {
        if (!isset(self::$_userByName[$username])) {
            $_userByName[$username] = User::model()->findByAttributes(array('username' => $username));
        }
        return $_userByName[$username];
    }

    /**
     * Return safe user data.
     * @param user id not required
     * @return user object or false
     */
    public static function users() {
        return User;
    }

}
