<?php

Yii::import('zii.widgets.CPortlet');

class TagCloud extends CPortlet
{
	public $maxTags=20;

	protected function renderContent()
	{
		$this->render('tagCloud');
	}
}