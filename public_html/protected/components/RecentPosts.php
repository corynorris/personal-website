<?php

Yii::import('zii.widgets.CPortlet');

class RecentPosts extends CPortlet
{
	
	public $maxPosts=3;
	public $truncationLength=50;

	public function getRecentPosts()
	{
		return Post::model()->findRecentPosts($this->maxPosts);
	}

	protected function renderContent()
	{
		$this->render('recentPosts');
	}
}