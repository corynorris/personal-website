<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
		'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
		'name'=>'LHG',

		// preloading 'log' component
		'preload'=>array('log','booster'),

		// autoloading model and component classes
		'import'=>array(
				'application.models.*',
				'application.components.*',

		),

		'modules'=>array(
				// uncomment the following to enable the Gii tool

				/*'gii'=>array(
						'class'=>'system.gii.GiiModule',
						'password'=>'Protoss!11',
						'generatorPaths' => array('booster.gii'),
						// If removed, Gii defaults to localhost only. Edit carefully to taste.
						'ipFilters'=>array('127.0.0.1','::1'),
				),*/

		),

		// application components
		'components'=>array(
				'user'=>array(
						// enable cookie-based authentication
						'allowAutoLogin'=>true,
						'loginUrl'=>array('/auth/login'),

				),
					
				'clientScript'=>array(
						'packages'=>array(
								'jquery'=>array(
										'baseUrl'=>'//ajax.googleapis.com/ajax/libs/jquery/1/',
										'js'=>array('jquery.min.js'),
								),
								'prettify'=>array(
										'baseUrl'=>'//google-code-prettify.googlecode.com/svn/loader/',
										'js'=>array('run_prettify.js'),
								)
						),
				),

				'booster' => array(
						'class' => 'ext.yiibooster.components.Booster',
				),

				// uncomment the following to enable URLs in path-format
				'urlManager'=>array(
						'urlFormat'=>'path',
						'rules'=>array(
								'<controller:\w+>/<id:\d+>'=>'<controller>/view',
								'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
								'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
						),
				),

				'db'=>array(
						'connectionString' => 'mysql:host=localhost;dbname=largehad_lhg',
						'emulatePrepare' => true,
						'tablePrefix' =>'tbl_',
						'username' => 'largehad_root',
						'password' => 'Zerg1616',
						'charset' => 'utf8',
				),

				'errorHandler'=>array(
						// use 'site/error' action to display errors
						'errorAction'=>'site/error',
				),
				'log'=>array(
						'class'=>'CLogRouter',
						'routes'=>array(
								array(
										'class'=>'CFileLogRoute',
										'levels'=>'error, warning',
								),
								// uncomment the following to show log messages on web pages
/*
 array(
 		'class'=>'CWebLogRoute',
 ),*/

						),
				),
		),

		// application-level parameters that can be accessed
		// using Yii::app()->params['paramName']
		'params'=>require(dirname(__FILE__).'/params.php'),
);
