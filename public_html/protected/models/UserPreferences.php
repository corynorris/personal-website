<?php

/**
 * This is the model class for table "{{user_preferences}}".
 *
 * The followings are the available columns in table '{{user_preferences}}':
 * @property integer $id
 * @property integer $games
 * @property integer $tutorials
 * @property string $activ_key
 * @property string $email
 * @property string $create_at
 * @property integer $status
 */
class UserPreferences extends CActiveRecord
{
	const STATUS_NOACTIVE = 0;
	const STATUS_ACTIVE = 1;
	const STATUS_UNSUBSCRIBED = -1;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user_preferences}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array('games, tutorials, activ_key, email, create_at, status', 'required'),
				array('games, tutorials, status', 'numerical', 'integerOnly'=>true),
				array('activ_key, email', 'length', 'max'=>128),
				array('email','email'),
				// The following rule is used by search().
				// @todo Please remove those attributes that should not be searched.
				array('id, games, tutorials, activ_key, email, create_at, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
				'id' => 'ID',
				'games' => 'Games',
				'tutorials' => 'Tutorials',
				'activ_key' => 'Activ Key',
				'email' => 'Email',
				'create_at' => 'Created At',
				'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('games',$this->games);
		$criteria->compare('tutorials',$this->tutorials);
		$criteria->compare('activ_key',$this->activ_key,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('create_at',$this->create_at,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
		));
	}

	public function scopes() {
		return array(
				'notsafe' => array(
						'select' => 'id, games, tutorials, activ_key, email, create_at, status',
				),
		);
	}


	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord){
				$this->create_time=time();
				$this->status = self::STATUS_NOACTIVE;
			}
			return true;
		}
		else
			return false;
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserPreferences the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
