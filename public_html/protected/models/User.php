<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $activkey
 * @property string $create_at
 * @property string $lastvisit_at
 * @property integer $superuser
 * @property integer $status
 *
 */
class User extends CActiveRecord {

    const STATUS_NOACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_BANNED = -1;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user}}';
	}


    /**
     * The followings are the available columns in table 'users':
     * @var integer $id
     * @var string $username
     * @var string $salt
     * @var string $password
     * @var string $email
     * @var string $activkey
     * @var integer $superuser
     * @var integer $status
     * @var timestamp $create_at
     * @var timestamp $lastvisit_at
     */

    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.CConsoleApplication
        return ((Yii::app()->user->id == $this->id) ? array(
                    array('username,  email', 'required'),
                    //array('username, password, salt', 'required', 'on'=>'login'),
                    array('username', 'length', 'max' => 20, 'min' => 3, 'message' => Helper::t("Incorrect username (length between 3 and 20 characters).")),
                    array('email', 'email'),
                    array('username', 'unique', 'message' => Helper::t("This user's name already exists.")),
                    array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u', 'message' => Helper::t("Incorrect symbols (A-z0-9).")),
                    array('email', 'unique', 'message' => Helper::t("This user's email address already exists.")),
                        ) : array());
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
      
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Helper::t("Id"),
            'username' => Helper::t("Username"),
            'password' => Helper::t("Password"),
            'verifyPassword' => Helper::t("Retype Password"),
            'email' => Helper::t("E-mail"),
            'verifyCode' => Helper::t("Verification Code"),
            'activkey' => Helper::t("activation key"),
            'create_at' => Helper::t("Registration date"),
            'lastvisit_at' => Helper::t("Last visit"),
            'superuser' => Helper::t("Superuser"),
            'status' => Helper::t("Status"),
        );
    }

    public function scopes() {
        return array(
            'active' => array(
                'condition' => 'status=' . self::STATUS_ACTIVE,
            ),
            'notactive' => array(
                'condition' => 'status=' . self::STATUS_NOACTIVE,
            ),
            'banned' => array(
                'condition' => 'status=' . self::STATUS_BANNED,
            ),
            'superuser' => array(
                'condition' => 'superuser=1',
            ),
            'notsafe' => array(
                'select' => 'id, username, password, email, activkey, create_at, lastvisit_at, superuser, status',
            ),
        );
    }


    public static function itemAlias($type, $code = NULL) {
        $_items = array(
            'UserStatus' => array(
                self::STATUS_NOACTIVE => Helper::t('Not active'),
                self::STATUS_ACTIVE => Helper::t('Active'),
                self::STATUS_BANNED => Helper::t('Banned'),
            ),
            'AdminStatus' => array(
                '0' => Helper::t('No'),
                '1' => Helper::t('Yes'),
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('password', $this->password);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('activkey', $this->activkey);
        $criteria->compare('create_at', $this->create_at);
        $criteria->compare('lastvisit_at', $this->lastvisit_at);
        $criteria->compare('superuser', $this->superuser);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 15,
            ),
        ));
    }

    public function afterSave() {
        //if the model is changed, update the session user name
        parent::afterSave();
        if (!$this->isNewRecord)
            Yii::app()->user->name = $this->username;
    }

    public function getCreatetime() {
        return strtotime($this->create_at);
    }

    public function setCreatetime($value) {
        $this->create_at = date('Y-m-d H:i:s', $value);
    }

    public function getLastvisit() {
        return strtotime($this->lastvisit_at);
    }

    public function setLastvisit($value) {
        $this->lastvisit_at = date('Y-m-d H:i:s', $value);
    }
    

}
