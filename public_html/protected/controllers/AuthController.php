<?php

/*
 * To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

/**
 * Description of AuthController
 *
 * @author Cory
 */
class AuthController extends Controller {


	public $defaultAction = 'login';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
				'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		return array(
				array('allow',
						'actions'=>array('login'),
						'users'=>array('*'),
				),
				array('allow', // allow all users to perform 'index' and 'view' actions
						'users' => array('@'),
				),
				array('deny',  // deny all users
						'users'=>array('*'),
				),
		);
	}

	public function actionGenPass($password) {
		echo $password . "<br>";

		$hash = Helper::encrypting($password) . "<br>";
		echo $hash;
		echo Helper::decrypting($password, $hash) . "<br>";
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin() {
		// If the user isn't logged in, show them the login page
		if (Yii::app()->user->isGuest) {

			$model = new LoginForm();
			// collect user input data
			if (isset($_POST['LoginForm'])) {
				$model->attributes = $_POST['LoginForm'];

				// validate user input and redirect to previous page if valid
				if ($model->validate()) {
					// Store their last visit time
					$this->lastVisit();
					// Return them to the return url if it's set.  Otherwise, go to their profile.
					$returnUrl = Yii::app()->user->returnUrl;
					if (strpos($returnUrl, 'index.php/') !== FALSE) {
						$this->redirect($returnUrl);
					} else {
						$this->redirect(Helper::$returnUrl);
					}
				} else {
					Yii::app()->user->setFlash('error', 'Some form items were not filled out correctly.');
				}
			}
			// display the login form
			$this->render('login', array('model' => $model));
		} else {
			$this->redirect(Yii::app()->user->returnUrl);
		}
	}

	public function actionLogout() {
		Yii::app()->user->logout();
		$this->redirect(Helper::$returnLogoutUrl);
	}



	private function lastVisit() {
		$lastVisit = User::model()->findByPk(Yii::app()->user->id);
		$lastVisit->lastvisit_at = date('Y-m-d H:i:s');
		$lastVisit->save();
	}

}
