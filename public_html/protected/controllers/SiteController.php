<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
				// captcha action renders the CAPTCHA image displayed on the contact page
				'captcha'=>array(
						'class'=>'CCaptchaAction',
						'backColor'=>0xFFFFFF,
				),
				// page action renders "static" pages stored under 'protected/views/site/pages'
				// They can be accessed via: index.php?r=site/page&view=FileName
				'page'=>array(
						'class'=>'CViewAction',
				),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$model = New UserPreferences();

		if(isset($_POST['ajax']) && $_POST['ajax']==='user-preferences-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}


		if(isset($_POST['UserPreferences']))
		{
			$user = Yii::app()->getComponent('user');

			$model->attributes=$_POST['UserPreferences'];
			$model->activ_key = Helper::encrypting(microtime() . $model->email);
			
			if($model->save()){
				$user->setFlash(
						'success',
						'<strong>Success</strong> You\'ll receive an email to verify your address shortly!'
				);

				$activation_url = $this->createAbsoluteUrl('/userPreferences/activate', array("activkey" => $model->activ_key, "email" => $model->email));
				Helper::sendMail($model->email, Yii::t('app', "Hi from {site_name}! We don't want to spam people, so we're asking that you please verify your email! ", array('{site_name}' => Yii::app()->name)), Yii::t('app', "Please activate you account go to {activation_url}", array('{activation_url}' => $activation_url)));

				Yii::app()->session['subscribed'] = true;
			} else
			{
				$user->setFlash(
						'danger',
						'<strong>Whoops!</strong> Your email could not be saved!'
				);
			}
		}

		$this->render('index',array(
				'model'=>$model,
		));
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionAntSimulator()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		if (!isset(Yii::app()->session['showAlert']))
			Yii::app()->session['showAlert'] = true;

		if (Yii::app()->session['showAlert'] == true)
		{
			$user = Yii::app()->getComponent('user');
			$user->setFlash(
					'info',
					'<strong>Please Note</strong> this game is currently a work in progress'
			);
		}
		$this->render('ant-simulator');
	}


	public function actionTest() {
		$this->render('/test/build');
	}

	public function actionDisableAlert()
	{
		if(isset($_POST['showAlert']))
		{
			Yii::app()->session['showAlert'] = false;
		}
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionWobbleRocket()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('wobble-rocket');
	}


	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
						"Reply-To: {$model->email}\r\n".
						"MIME-Version: 1.0\r\n".
						"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
			
				$model = new ContactForm;

				$user = Yii::app()->getComponent('user');
				$user->setFlash(
						'success',
						'Thank you for contacting us. We will respond to you as soon as possible.'
				);
			
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionActivate()
	{
		$user = Yii::app()->getComponent('user');
		$model = new UserPreferences();
		if (!isset($_GET['activkey']) || !isset($_GET['email']))
		{
			// error
			Yii::app()->session['subscribed'] = false;

			$user->setFlash(
					'danger',
					'<strong>Invalid Activation Url!</strong> Please use the contact form, or try subscribing again!'
			);

		}
		else
		{

			$email = $_GET['email'];
			$activ_key = $_GET['activkey'];

			$update = UserPreferences::model()->notsafe()->findByAttributes(array('email' => $email, 'activ_key'=>$activ_key));

			if (!isset($update))
			{

				$user->setFlash(
						'danger',
						'<strong>Activation Failed!</strong> Please use the contact form, or try subscribing again!'
				);

				Yii::app()->session['subscribed'] = false;

			} else
			{
				$update->status = UserPreferences::STATUS_ACTIVE;
				if ($update->save())
				{
					$user->setFlash(
							'success',
							'<strong>Activation Succeded!</strong> You\'ll now receive updates!'
					);
					Yii::app()->session['subscribed'] = true;
				} else 
				{
					$user->setFlash(
							'danger',
							'<strong>Save Failed!</strong> Please try again later!'
					);
					
					Yii::app()->session['subscribed'] = false;
				}
			}

		}

		$this->render('index',array(
				'model'=>$model,
		));
	}


}
